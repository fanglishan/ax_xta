% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2023 Fang
% Author: Fang
%
% This function calculates the general forms of X for 
% equation AX=X^{T}A, where A is input mannually.

addpath(genpath('.\lib'));

A = [0 1 0 0;
     1 2 2 0;
     0 2 3 1;
     0 0 1 2];

B = [0 1 0 0;
     1 3 2 0;
     0 2 3 1;
     0 0 1 2];

A = [3 2 0;
     2 2 1;
     0 1 0];

B = [3 2 0;
     2 3 1;
     0 1 0]; 

CalculateXForm(A,B)

% Obtain pattern of X for given A
function CalculateXForm(A,B)

    % Adjust variable n using matrix dimension
    dim = length(A);
    disp(A)
    disp(B)
    [M_rref, p] = SolveMultipleSystem(A,B,dim);
    M_rref
    p
    X = OutputX(dim, M_rref, p);


    fprintf("# free variable: %d \n", dim^2-max(size(p)));
    fprintf("General form of X \n");
    
    disp(X)
end