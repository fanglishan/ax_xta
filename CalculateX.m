% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function calculates the general forms of X for 
% equation AX=X^{T}A, where A is input mannually.

addpath(genpath('.\lib'));

A = [0 1 0 0;
     1 2 2 0;
     0 2 3 1;
     0 0 1 2];

CalculateXForm(A)

% Obtain pattern of X for given A
function CalculateXForm(A)

    % Adjust variable n using matrix dimension
    dim = length(A);
    disp(A)
    [M_rref, p] = SolveSystem(A,dim);

    X = OutputX(dim, M_rref, p);
    
    fprintf("# free variable: %d \n", dim^2-max(size(p)));
    fprintf("General form of X \n");
    
    disp(X)
end