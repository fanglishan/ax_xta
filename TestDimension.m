% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function tests if the dimension of solution X is consistent with
% given calculated dimensions. The matrix A takes the form of a combination
% of two canonical forms J,G or H.
%
% This function has not been fully implemented/tested.


addpath(genpath('.\lib'));

no_test = 6;
ValidateDimension(no_test);

%type = [1,2,3];
%n = [3,4,2];
%lambda = [2,0,-1];
%A = BuildMatrix(type,n,lambda);
%disp(A)

% Validate theoretical results about dimension of solution
function ValidateDimension(no_test)
%    TestJ(no_test);
%    TestG(no_test);
%    TestH(no_test);
    TestJJ(no_test);
%    TestGG(no_test);
%    TestHH(no_test);
end


% Test case for J_k(0)
function TestJ(no_test)
    type = 1;
    lambda = 0;

    count = CompareDimension(type,lambda,no_test);

    if count == 0
        fprintf("Test J passed \n");
    else
        fprintf("Test J failed with %d cases \n", count);
    end
end


% Test case for G_k
function TestG(no_test)
    type = 2;
    lambda = -1;

    count = CompareDimension(type,lambda,no_test);

    if count == 0
        fprintf("Test G passed \n");
    else
        fprintf("Test G failed with %d cases \n", count);
    end
end


% Test case for H_2k(0)
function TestH(no_test)
    type = 2;
    lambda = 0;

    count = CompareDimension(type,lambda,no_test);

    if count == 0
        fprintf("Test H passed \n");
    else
        fprintf("Test H failed with %d cases \n", count);
    end
    
end


% Test case 1: combination of J_k(0)
function TestJJ(no_test)

    type = [1 1];
    n = [2 3];
    lambda = [0 0];
%    type = [1 1;1 1;1 1;1 1;1 1];
%    n = [2 3;4 5;6 2;7 4;4 4];
%    lambda = [0 0;0 0;0 0;0 0;0 0];
    count = 0;

    % Test using different sizes of A matrices
    for i=1:length(type(:,1))
        type(i,:)
        n(i,:)
        lambda(i,:)

        % Calculate expected dimension
        dim1 = CalcDimension(type(i,:), n(i,:), lambda(i,:));

        % Calculate expected dimension
        dim2 = CalcPatternDim(type(i,:), n(i,:), lambda(i,:));
        %fprintf("Size %d with dim1 %d vs dim2 %d \n", n, dim1, dim2);
        
        if dim1 ~= dim2
            count = count + 1;
            fprintf("Size %d with dim1 %d vs dim2 %d \n", n, dim1, dim2);
        end
    end

    if count == 0
        fprintf("Test 1 J & J passed \n");
    else
        fprintf("Test 1 J & J failed with %d cases \n", count);
    end
end


% Test case 2: combination of G_k
function TestGG(no_test)
    type = 2;
    lambda = 0;

    count = CompareDimension(type,lambda,no_test);

    if count == 0
        fprintf("Test 2 G & G passed \n");
    else
        fprintf("Test 2 G & G failed with %d cases \n", count);
    end
end


% Test case 3: combination of H_2k(0)
function TestHH(no_test)
    type = 2;
    lambda = 0;

    count = CompareDimension(type,lambda,no_test);

    if count == 0
        fprintf("Test 3 H & H passed \n");
    else
        fprintf("Test 3 H & H failed with %d cases \n", count);
    end
    
end
