% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function plots general forms of X in equation A*X=X^{T}*A for
% different sizes of the system. So far it is only applicable for basic J,
% G or H matrix.

addpath(genpath('.\lib'));

n = 4;
lambda = 0;
PlotXforJ(n,lambda)
%PlotXforG(n)
%PlotXforH(n,lambda)


% Plot forms of X for A=J
function PlotXforJ(dim,lambda)
    fprintf("Example Jordan matrix with n=%d \n", 5);
    A = MatrixJ(5,lambda);
    disp(A)

    for i=2:dim
        n = i;
        A = MatrixJ(n,lambda);
        n = length(A);
    
        [M_rref, p] = SolveSystem(A,n);
    
        X = OutputX(n, M_rref, p);
        fprintf("Dimension: %d \n", i);
        disp(X)
    end
end


% Plot forms of X for A=G
function PlotXforG(dim)
    fprintf("Example Gamma matrix with n=%d \n", 5);
    A = MatrixG(5);
    disp(A)

    for i=3:dim
        n = i;
        A = MatrixG(n);
        n = length(A);
    
        [M_rref, p] = SolveSystem(A,n);
    
        X = OutputX(n, M_rref, p);
        fprintf("Dimension: %d \n", i);
        disp(X)
    end
end


% Plot forms of X for A=H
function PlotXforH(dim,lambda)
    fprintf("Example H matrix with n=%d \n", 5);
    A = MatrixH(5,lambda);

    for i=3:dim
        n = i;
        A = MatrixH(n,lambda);
        n = length(A);
    
        [M_rref, p] = SolveSystem(A,n);
    
        X = OutputX(n, M_rref, p);
        fprintf("Dimension: %d \n", n);
        disp(X);
    end
end