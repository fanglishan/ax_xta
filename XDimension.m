% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function extract general forms of X for equation AX=X^{T}A, where A
% takes a canonical form defined by several parameters.
% Three canonical forms： 1-J_k(0), 2-G_k, 3-H_2k(0)

addpath(genpath('.\lib'));

% the upper limit of size of testing
finish = 4;

type = [2 3];
lambda = [0 1];

ValidateInput(type,n,lambda);

dim_matrix = CalcPatternDim(type,lambda,finish);



% Calculate dimension of X for a system consisting of two canonical forms
% with various sizes
function dim_matrix = CalcPatternDim(type,lambda,finish)
    dim_matrix = zeros(finish);
    for i=1:finish
        for j=1:finish

            % Do not calculate J or G if k=1
            if i == 1 && type(1) ~= 3 || j == 1 && type(2) ~= 3
                continue
            end
            n = [i,j];

            dimension = obtainPatternDim(type, n, lambda);

            dim_matrix(i,j) = dimension;
        end
    end

    fprintf("Three canonical forms： 1-J_k(0), 2-G_k, 3-H_2k(lambda)\n")
    fprintf("A of AX=X^{T}A consists of\n")
    for i=1:length(type)
        fprintf("Type %d with size %d and lambda %d \n",type(i),n(i),lambda(i));
    end
    fprintf("The dimension of resulting X is shown below\n");
    fprintf("i-th and j-th entry represent size k of first and second canonical forms, respectively\n");
    disp(dim_matrix)
end


% Obtain dimension of X for given system 
function dimension = obtainPatternDim(type, n, lambda)
    A = BuildMatrix(type,n,lambda); 
    dim = length(A);
    [M_rref, p] = SolveSystem(A,dim);
    dimension = dim^2-max(size(p));
end