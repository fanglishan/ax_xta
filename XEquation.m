% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
% 
% This function tests if a given matrix satisfies equation A*X=X^{T}*A.
% It contains multiple small tests for specific scenarios, which can be
% easily modified.

addpath(genpath('.\lib'));

test2()
test3()
test4()
test5()


function test5()
    type = [2 2];
    n = [2 2];
    lambda = [0 0];
    
    X = [4 0 2 3;
         0 4 0 2;
         2 -3 1 0;
         0 2 0 1];
    %{
    X = [7 8 0 2 6 0;
         0 7 0 0 5 0;
         0 8 7 0 4 2;
         5 4 0 1 3 0;
         0 2 0 0 1 0;
         0 6 5 0 3 1];
    %}
    residual = CheckResidual(X, type, n, lambda);
    disp(residual);
end


function test4()
    type = [1 1];
    n = [3 3];
    lambda = [0 0];
    %{
    X = [4 0 3 0;
         0 4 0 2;
         2 0 1 0;
         0 3 0 1];
    %}
    X = [7 8 0 2 6 0;
         0 7 0 0 5 0;
         0 8 7 0 4 2;
         5 4 0 1 3 0;
         0 2 0 0 1 0;
         0 6 5 0 3 1];
    residual = CheckResidual(X, type, n, lambda);
    disp(residual);
end


function test3()
    type = [3];
    n = [4];
    lambda = [1];
%    X = eye(8);
%{
    X = [1 2 3 4 0 0 0 0;
         0 1 2 3 0 0 0 0;
         0 0 1 2 0 0 0 0;
         0 0 0 1 0 0 0 0;
         0 0 0 0 1 0 0 0;
         0 0 0 0 2 1 0 0;
         0 0 0 0 3 2 1 0;
         0 0 0 0 4 3 2 1];   
%}
    X = [1 2 3 4 7 8 8 0;
         0 1 2 3 0 -8 0 0;
         0 0 1 2 8 0 0 0;
         0 0 0 1 0 0 0 0;
         0 0 0 0 1 0 0 0;
         0 0 0 5 2 1 0 0;
         0 0 -5 0 3 2 1 0;
         0 5 5 6 4 3 2 1];
%{
    X = [1 2 3 4 0 0 0 0;
         0 1 2 3 0 0 0 0;
         0 0 1 2 0 0 0 0;
         0 0 0 1 0 0 0 0;
         0 0 0 -2 1 0 0 0;
         0 0 2 1 2 1 0 0;
         0 -2 1 0 3 2 1 0;
         2 -3 1 0 4 3 2 1];
%}
    residual = CheckResidual(X, type, n, lambda);
    disp(residual);
end


function test2()
    type = [2];
    n = [4];
    lambda = [0];
    X = [1 0 1 0; 0 1 0 1; 0 0 1 0; 0 0 0 1];
    
    residual = CheckResidual(X, type, n, lambda);
end


% Calculate the residual of solution for this system
% It should return 0 if it is a solution.
function residual = CheckResidual(X, type, n, lambda)

    A = BuildMatrix(type,n,lambda);

    if length(A) ~= length(X)
        fprintf("Dimension: %d \n", dim);
    end

    residual = A*X-transpose(X)*A;

    fprintf("A\n");
    disp(A);
    fprintf("X\n");
    disp(X);
    fprintf("residual\n");
    disp(residual);
    if any(any(residual)) 
        fprintf("The given X does not satisfy the equation \n");
    else
        fprintf("The given X satisfies the equation \n");
    end
end