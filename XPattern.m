% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function extract general forms of X for equation AX=X^{T}A, where A
% takes a canonical form defined by several parameters.
% Three canonical forms： 1-J_k(0), 2-G_k, 3-H_2k(0)

addpath(genpath('.\lib'));

type = [1 1 1];
n = [3 3 3];
lambda = [0 0 0];

ValidateInput(type,n,lambda);

ObtainPattern(type, n, lambda);


% Obtain pattern of X for given system
function ObtainPattern(type, n, lambda)

    A = BuildMatrix(type,n,lambda);

    % Adjust variable n using matrix dimension
    dim = length(A);
    disp(A)
    [M_rref, p] = SolveSystem(A,dim);

    X = OutputX(dim, M_rref, p);
    
    for i=1:length(type)
        fprintf("Type %d with size %d and lambda %d \n",type(i),n(i),lambda(i));
    end
    fprintf("# free variable: %d \n", dim^2-max(size(p)));
    fprintf("General form of X \n");
    
    disp(X)
end