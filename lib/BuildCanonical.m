% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function builds a canonical matrix J, G or H given input variables.


function matrix = BuildCanonical(type,n,lambda)
    if type == 1
        matrix = MatrixJ(n,lambda);
    elseif type == 2
        matrix = MatrixG(n);
    elseif type == 3
        matrix = MatrixH(n,lambda);
    else
        fprintf("Inconsistent canonical form: %d\n",type);
    end
end
