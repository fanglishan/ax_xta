% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function builds a matrix according to given specifications, which
% make it a combination of J, G and H matrix.


function matrix = BuildMatrix(type,n,lambda)
    size = length(type);
    if size == length(n) && size == length(lambda) && size > 0
        matrix = BuildCanonical(type(1),n(1),lambda(1));
        
        % Build a new matrix by added up block matrix iteratively
        for i=2:size
            new_matrix = BuildCanonical(type(i),n(i),lambda(i));
            matrix = blkdiag(matrix,new_matrix);
        end
    else
        fprintf("Inconsistent sizes %d, %d and %d \n",...
                length(type),length(n),length(lambda));
    end
end





