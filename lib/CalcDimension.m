% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function calculates dimension of X for equation A*X=X^{T}*A.
% Currently it includes cases where A is a combination of two types of 
% canonical forms (or one type with different settings). These theoretical
% results need to be validated before using for extensive tests.
%
% This function has not been fully implemented/tested.


function dim = CalcDimension(type,n,lambda)

    dim = 0;
    if type == 1

        for i=1:length(n)
            dim = dim + ceil(n(i)/2);
        end

    elseif type == 2
        for i=1:length(n)
            dim = dim + ceil(n(i)/2);
        end
    elseif type == 3
        for i=1:length(n)
            dim = dim + n(i);
            if lambda(i) == 1 || lambda(i) == -1
                dim = dim + floor(n(i)/2);
            end
        end
    elseif type == 4
        if length(n) == 2
            if n(1) >= n(2)
                i=1;
                j=2;
            else
                i=2;
                j=1;
            end
            if ~mod(n(j),2)
                dim = n(j);
            elseif mod(n(i),2) && m~=n
                dim = n(i);
            elseif mod(n(j),2) && m==n
                dim = n(j)+1;
            else
                fprintf("Case not considered")
            end
        else
            dim = ceil(n(1)/2);    % More complex cases
            fprintf("Require further implementation")
        end
        
    elseif type == 5
        if length(n) == 2
            if mod(n(1),2) == mod(n(2),2)
                dim = min(n(1),n(2));
            else
                dim = 0;
            end
        else
            dim = ceil(n(1)/2);    % More complex cases
            fprintf("Require further implementation")
        end
    elseif type == 6
        if length(n) == 2
            if lambda(1) == lambda(2) && (lambda(1) == 1 || lambda(1) == -1)
                dim = 4*min(n(1),n(2));
            elseif lambda(1) == lambda(2) && lambda(1) ~= 1
                dim = 0;
            elseif lambda(1) ~= lambda(2) && lambda(1)*lambda(2) == 1
                dim = 0;
            elseif lambda(1) ~= lambda(2) && lambda(1)*lambda(2) ~= 1
                dim = 0;
            else
                fprintf("Case not considered")
            end
        else
            dim = ceil(n(1)/2);    % More complex cases
            fprintf("Require further implementation")
        end
    elseif type == 7
        if length(n) == 2
            if type(1) == 1 && type(2) == 2 && mod(n(1),2)
                dim = n(2);
            elseif type(2) == 1 && type(1) == 2 && mod(n(2),2)
                dim = n(1);
            elseif type(1) == 1 && ~mod(n(1),2)
                dim = 0;
            elseif type(2) == 1 && ~mod(n(2),2)
                dim = 0;
            else
                fprintf("Case not considered")
            end
        else
            dim = ceil(n(1)/2);    % More complex cases
            fprintf("Require further implementation")
        end
    elseif type == 8
        if length(n) == 2
            if type(1) == 1 && type(2) == 3 && mod(n(1),2)
                dim = 2*n(1);
            elseif type(2) == 1 && type(1) == 3 && mod(n(2),2)
                dim = 2*n(2);
            else
                fprintf("Case not considered")
            end
        else
            dim = ceil(n(1)/2);    % More complex cases
            fprintf("Require further implementation")
        end
    elseif type == 9
        if length(n) == 2
            if type(1) == 3 && type(2) == 2 && lambda(1) == (-1)^(n(2))
                dim = 2*min(n(1),n(2));
            elseif type(2) == 3 && type(1) == 2 && lambda(2) == (-1)^(n(1))
                dim = 2*min(n(1),n(2));
            elseif type(1) == 3 && type(2) == 2 && lambda(1) ~= (-1)^(n(2))
                dim = 0;
             elseif type(2) == 3 && type(1) == 2 && lambda(2) ~= (-1)^(n(1))
                dim = 2*min(n(1),n(2));
            else
                fprintf("Case not considered")
            end
        else
            dim = ceil(n(1)/2);    % More complex cases
            fprintf("Require further implementation")
        end
    end
end