% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function calculates the dimension of X.


% Build RREF of system and obtain number of free variables
function dim = CalcPatternDim(type,n,lambda)
    A = BuildMatrix(type,n,lambda);

    n = length(A);

    [M_rref,p] = SolveSystem(A,n);

    % Number of free variables
    dim = n^2-max(size(p));
end
