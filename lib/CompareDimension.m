% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function tests if the dimension of solution X is consistent with
% given calculated dimensions.


% Compare dimension obtained by theory and using RREF
function count = CompareDimension(type,lambda,no_test)
    count = 0;

    % Test using different sizes of A matrices
    for i=2:no_test

        % Calculate expected dimension
        dim1 = CalcDimension(type,i,lambda);

        % Calculate expected dimension
        dim2 = CalcPatternDim(type,i,lambda);
        %fprintf("Size %d with dim1 %d vs dim2 %d \n", n, dim1, dim2);
        
        if dim1 ~= dim2
            count = count + 1;
            fprintf("Size %d with dim1 %d vs dim2 %d \n", n, dim1, dim2);
        end
    end
end