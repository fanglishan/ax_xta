% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function builds a Gamma matrix. The detailed definition and 
% restrictions are provided in Article Canonical forms for complex matrix 
% congruence and ∗congruence.


function matrix = MatrixG(n)
    matrix = zeros(n);
    for i=1:n
        matrix(n-i+1,i) = -(-1.0)^i;
    end
    for i=1:n-1
        matrix(n-i+1,i+1) = -(-1.0)^i;
    end
end
