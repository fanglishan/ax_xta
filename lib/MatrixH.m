% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function builds a H matrix that is a skew sum of J matrix and an
% identity matrix. The detailed definition and restrictions are provided 
% in Article Canonical forms for complex matrix congruence and ∗congruence.


function matrix = MatrixH(n,lambda)
    matrix = zeros(2*n);
    for i=1:n
        matrix(i,n+i) = 1.0;
    end
    for i=1:n-1
        matrix(n+i,i+1) = 1.0;
    end
    for i=1:n
        matrix(n+i,i)=lambda;
    end
end
