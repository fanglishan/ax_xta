% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function builds a Jordan block matrix with eigenvalue lambda. The 
% detailed definition and restrictions are provided in Article Canonical 
% forms for complex matrix congruence and ∗congruence.


function matrix = MatrixJ(n,lambda)
    matrix = zeros(n);
    for i=1:n-1
        matrix(i,i+1) = 1.0;
    end
    for i=1:n
        matrix(i,i) = lambda;
    end
end

