% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function extracts information from RREF form of the system and
% output its general form, where free variables are denoted as x1,...,xn.


function X = OutputX(n,M_rref,p)
    % Threshold for nonzero entries
    tol = 1e-14;

    %= length(M_rref);
    dim = n^2;
    
    % Indexes free variables 
    variables = [];
    for i=1:dim
        index = find(p==i, 1);
        if isempty(index)
            variables(end+1) = i;
        end
    end
    variables
    X = string(zeros(n));
    symbols = ["x1","x2","x3","x4","x5","x6","x7","x8","x9","x10",...
        "x11","x12","x13","x14","x15","x16","x17","x18","x19","x20",...
        "x21","x22","x23","x24","x25","x26","x27","x28","x29","x30",...
        "x31","x32","x33","x34","x35","x36","x37","x38","x39","x40",...
        "x41","x42","x43","x44","x45","x46","x47","x48","x49","x50",...
        "x51","x52","x53","x54","x55","x56","x57","x58","x59","x60"];

    % Add symbols for free variables
    for i=1:length(variables)
        [j,k] = calc_index(variables(i),n);
        X(j,k) = symbols(length(variables)-i+1);
    end

    % Add symbols to other entries   
    for i=1:dim
        
        entry = find(p==i, 1);
        if isempty(entry)
            continue
        else
            index = locateEntry(M_rref,dim,i);
            if index == 0
                fprintf("incorrect index");
            end
        end
        
        [x,y] = calc_index(i, n);

%        fprintf("%d %d, %d %d \n",i,index,x,y);
        for j=1:length(variables)
            k = variables(j);
            if abs(M_rref(index,k)) > tol
                coef = -1.0*M_rref(index,k);
                
                % Combine coefficient with variable symbol
                if coef == 1 
                    str = symbols(length(variables)-j+1);
                elseif coef == -1
                    str = "-" + symbols(length(variables)-j+1);
                else
                    str = string(coef) + symbols(length(variables)-j+1);
                end

                % Replace or append to original string
                if X(x,y) == "0"
                    X(x,y) = str;
                else
                    X(x,y) = str + "+" + X(x,y);
                end
            end
        end
    end
    
    % The original system was implemented for A*X^{T}=X*A
    % Transform X of A*X^{T}=X*A to X of A*X=X^{T}*A
    X = transpose(X);
end


% Locate the row that contains 1 for given variable in RREF
function index = locateEntry(M_rref,n,i)
    index = 0;
    for j=1:n
        if M_rref(j,i) == 1
            index = j;
            break;
        end
    end 
end


% Calculates i,j position of an unknown variable in input matrix
function [i,j] = calc_index(input,n)
   i = ceil(input/n);
   j = input-(i-1)*n;
end
