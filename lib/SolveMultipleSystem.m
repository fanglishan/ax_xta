% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function builds a system and computes its RREF for A*X^{T}=X*A.
% The resulting X can be transformed to X of A*X=X^{T}*A by a transpose.


function [M_rref,p] = SolveMultipleSystem(A,B,n)
    M1 = buildSystem(A,n);
    M2 = buildSystem(B,n);

    M = [M1;M2];

    [M_rref, p] = rref(M);
end


% Builds a system of equations for A*X^{T}=X*A.
function matrix = buildSystem(A,n)
    matrix = zeros(n*n);
    
    count = 1;
    for i=1:n
        for j=1:n
            for k=1:n
                % Add entry value from left matrix
                index = (i-1)*n+k;
                matrix(count,index) = matrix(count,index) + A(k,j);
                % Add entry value from right matrix
                index = (j-1)*n+k;
                matrix(count,index) = matrix(count,index) - A(i,k);
            end
            count = count + 1;
        end
    end
end