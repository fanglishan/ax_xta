% The AX_XTA program solves the equation A*X=X^{T}*A.
% Copyright (C) 2022 Fang
% Author: Fang
%
% This function calculates dimension of X for equation A*X=X^{T}*A.
% Currently it includes cases where A is a combination of two types of 
% canonical forms (or one type with different settings).


function ValidateInput(type,n,lambda)

    % Check sizes
    if length(type) ~= length(lambda) || length(n) ~= length(lambda)
        fprintf("input variables different sizes");
    end

    % Check input canonical form type
    for i=1:length(type)
        if type(i) ~= 1 && type(i) ~= 2 && type(i) ~= 3
            fprintf("Inconsistent canonical form: %d\n",type(i));
        end
    end
end