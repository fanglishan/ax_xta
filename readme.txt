The AX_XTA program

This program solves the equation A*X=X^{T}*A. A is a combination of three canonical matrices J, G and H and X is the solution.
Common specifications include type of canonical form, size of each form and lambda/mu values in J and H matrices.

The basic functionality of this program can be accessed through several files, including
- CalculateX: output general forms of X for a specified A.
- TestDimension: test whether dimension of X is consistent with theoretical results (This function has not been fully implemented/tested).
- TestForms: output general forms of X for various sizes.
- XDimension: output dimensions of X for various sizes.
- XEquation: test if an X satisfy the equation.
- XPattern: output general forms of X when A consists of the three canonical forms.

folders:
- lib: functions provide lower level utility for this program.
- results: existing results including general forms or dimensions of X for different specified A matrices.





