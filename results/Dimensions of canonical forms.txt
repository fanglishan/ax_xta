============================
This file contains the dimension of a combination of two canonical forms with zero lambda and various k values
============================


Three canonical forms： 1-J_k(0), 2-G_k, 3-H_2k(lambda)
A of AX=X^{T}A consists of
Type 1 with size 10 and lambda 0 
Type 1 with size 10 and lambda 0 
The dimension of resulting X is shown below
i-th and j-th entry represent size k of first and second canonical forms, respectively
     0     0     0     0     0     0     0     0     0     0
     0     4     5     5     6     6     7     7     8     8
     0     5     8     8    10    11    13    14    16    17
     0     5     8     8     9     9    10    10    11    11
     0     6    10     9    12    12    14    15    17    18
     0     6    11     9    12    12    13    13    14    14
     0     7    13    10    14    13    16    16    18    19
     0     7    14    10    15    13    16    16    17    17
     0     8    16    11    17    14    18    17    20    20
     0     8    17    11    18    14    19    17    20    20


Three canonical forms： 1-J_k(0), 2-G_k, 3-H_2k(lambda)
A of AX=X^{T}A consists of
Type 2 with size 10 and lambda 0 
Type 2 with size 10 and lambda 0 
The dimension of resulting X is shown below
i-th and j-th entry represent size k of first and second canonical forms, respectively
     0     0     0     0     0     0     0     0     0     0
     0     4     3     5     4     6     5     7     6     8
     0     3     7     4     8     5     9     6    10     7
     0     5     4     8     5     9     6    10     7    11
     0     4     8     5    11     6    12     7    13     8
     0     6     5     9     6    12     7    13     8    14
     0     5     9     6    12     7    15     8    16     9
     0     7     6    10     7    13     8    16     9    17
     0     6    10     7    13     8    16     9    19    10
     0     8     7    11     8    14     9    17    10    20


Three canonical forms： 1-J_k(0), 2-G_k, 3-H_2k(lambda)
A of AX=X^{T}A consists of
Type 3 with size 10 and lambda 0 
Type 3 with size 10 and lambda 0 
The dimension of resulting X is shown below
i-th and j-th entry represent size k of first and second canonical forms, respectively
     4     5     6     7     8     9    10    11    12    13
     5     8     9    10    11    12    13    14    15    16
     6     9    12    13    14    15    16    17    18    19
     7    10    13    16    17    18    19    20    21    22
     8    11    14    17    20    21    22    23    24    25
     9    12    15    18    21    24    25    26    27    28
    10    13    16    19    22    25    28    29    30    31
    11    14    17    20    23    26    29    32    33    34
    12    15    18    21    24    27    30    33    36    37
    13    16    19    22    25    28    31    34    37    40


Three canonical forms： 1-J_k(0), 2-G_k, 3-H_2k(lambda)
A of AX=X^{T}A consists of
Type 1 with size 10 and lambda 0 
Type 2 with size 10 and lambda 0 
The dimension of resulting X is shown below
i-th and j-th entry represent size k of first and second canonical forms, respectively
     0     0     0     0     0     0     0     0     0     0
     0     2     3     3     4     4     5     5     6     6
     0     5     7     8    10    11   13    14   16    17
     0     3     4     4     5     5     6      6     7     7
     0     6     8     9    11    12   14    15   17    18
     0     4     5     5     6      6     7     7     8     8
     0     7     9    10    12   13    15   16   18    19
     0     5     6     6     7      7     8     8     9     9
     0     8    10    11    13   14    16  17   19    20
     0     6     7     7     8      8     9     9    10    10


Three canonical forms： 1-J_k(0), 2-G_k, 3-H_2k(lambda)
A of AX=X^{T}A consists of
Type 1 with size 10 and lambda 0 
Type 3 with size 10 and lambda 0 
The dimension of resulting X is shown below
i-th and j-th entry represent size k of first and second canonical forms, respectively
     0     0     0     0     0     0     0     0     0     0
     4     5     6     7     8     9    10    11    12    13
     5     8    11    14    17    20    23    26    29    32
     5     8     9    10    11    12    13    14    15    16
     6     9    12    15    18    21    24    27    30    33
     6     9    12    13    14    15    16    17    18    19
     7    10    13    16    19    22    25    28    31    34
     7    10    13    16    17    18    19    20    21    22
     8    11    14    17    20    23    26    29    32    35
     8    11    14    17    20    21    22    23    24    25


Three canonical forms： 1-J_k(0), 2-G_k, 3-H_2k(lambda)
A of AX=X^{T}A consists of
Type 2 with size 10 and lambda 0 
Type 3 with size 10 and lambda 0 
The dimension of resulting X is shown below
i-th and j-th entry represent size k of first and second canonical forms, respectively
     0     0     0     0     0     0     0     0     0     0
     2     3     4     5     6     7     8     9    10    11
     3     4     5     6     7     8     9    10    11    12
     3     4     5     6     7     8     9    10    11    12
     4     5     6     7     8     9    10    11    12    13
     4     5     6     7     8     9    10    11    12    13
     5     6     7     8     9    10    11    12    13    14
     5     6     7     8     9    10    11    12    13    14
     6     7     8     9    10    11    12    13    14    15
     6     7     8     9    10    11    12    13    14    15
