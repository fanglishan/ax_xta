2% Three canonical forms： 1-J_k(0), 2-G_k, 3-H_2k(0)

Type 1 with size 2 and lambda 0 
Type 2 with size 2 and lambda 0 
# free variable: 2 
General form of X 
    "x2"    "0"     "0"     "0" 
    "0"     "x2"    "0"     "0" 
    "0"     "0"     "x1"    "0" 
    "0"     "0"     "0"     "x1"


Type 1 with size 2 and lambda 0 
Type 2 with size 3 and lambda 0 
# free variable: 3 
General form of X 
    "x3"    "0"     "0"     "0"     "0" 
    "0"     "x3"    "0"     "0"     "0" 
    "0"     "0"     "x1"    "0"     "x2"
    "0"     "0"     "0"     "x1"    "0" 
    "0"     "0"     "0"     "0"     "x1"


Type 1 with size 3 and lambda 0 
Type 2 with size 2 and lambda 0 
# free variable: 5 
General form of X 
    "x4"    "x5"              "0"     "-0.5x2+-0.5x3"    "x3"
    "0"     "x4"              "0"     "0"                "0" 
    "0"     "x5"              "x4"    "0.5x2+0.5x3"      "x2"
    "0"     "-0.5x2+0.5x3"    "0"     "x1"               "0" 
    "0"     "0.5x2+0.5x3"     "0"     "0"                "x1"


Type 1 with size 3 and lambda 0 
Type 2 with size 3 and lambda 0 
# free variable: 7 
General form of X 
    "x6"    "x7"                 "0"     "-0.5x3+0.5x4+x5"    "x3+-x4+-x5"    "x4"
    "0"     "x6"                 "0"     "0"                  "0"             "0" 
    "0"     "x7"                 "x6"    "-0.5x3+0.5x4+x5"    "x5"            "x3"
    "0"     "0.5x3+0.5x4"        "0"     "x1"                 "0"             "x2"
    "0"     "-0.5x3+0.5x4"       "0"     "0"                  "x1"            "0" 
    "0"     "-0.5x3+0.5x4+x5"    "0"     "0"                  "0"             "x1"


Type 1 with size 3 and lambda 0 
Type 2 with size 4 and lambda 0 
# free variable: 8 
General form of X 
    "x7"    "x8"                    "0"     "-0.5x3+-0.5x4+x5+…"    "x3+x4+-2x5+x6"    "-x3+-x4+x5"    "x4"
    "0"     "x7"                    "0"     "0"                      "0"                "0"             "0" 
    "0"     "x8"                    "x7"    "0.5x3+0.5x4+-x5+x6"     "x6"               "x5"            "x3"
    "0"     "-0.5x3+0.5x4"          "0"     "x1"                     "0"                "x2"            "0" 
    "0"     "0.5x3+0.5x4"           "0"     "0"                      "x1"               "0"             "x2"
    "0"     "0.5x3+0.5x4+-x5"       "0"     "0"                      "0"                "x1"            "0" 
    "0"     "0.5x3+0.5x4+-x5+x6"    "0"     "0"                      "0"                "0"             "x1"



Type 1 with size 4 and lambda 0 
Type 2 with size 3 and lambda 0 
# free variable: 4 
General form of X 
    "x3"    "0"     "x4"    "0"     "0"     "0"     "0" 
    "0"     "x3"    "0"     "0"     "0"     "0"     "0" 
    "0"     "0"     "x3"    "0"     "0"     "0"     "0" 
    "0"     "x4"    "0"     "x3"    "0"     "0"     "0" 
    "0"     "0"     "0"     "0"     "x1"    "0"     "x2"
    "0"     "0"     "0"     "0"     "0"     "x1"    "0" 
    "0"     "0"     "0"     "0"     "0"     "0"     "x1"


Type 1 with size 4 and lambda 0 
Type 2 with size 4 and lambda 0 
# free variable: 4 
General form of X 
    "x3"    "0"     "x4"    "0"     "0"     "0"     "0"     "0" 
    "0"     "x3"    "0"     "0"     "0"     "0"     "0"     "0" 
    "0"     "0"     "x3"    "0"     "0"     "0"     "0"     "0" 
    "0"     "x4"    "0"     "x3"    "0"     "0"     "0"     "0" 
    "0"     "0"     "0"     "0"     "x1"    "0"     "x2"    "0" 
    "0"     "0"     "0"     "0"     "0"     "x1"    "0"     "x2"
    "0"     "0"     "0"     "0"     "0"     "0"     "x1"    "0" 
    "0"     "0"     "0"     "0"     "0"     "0"     "0"     "x1"